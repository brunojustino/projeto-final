#coding: utf-8


import os, sys
import mongoconfig as config
from facedetection import TakeSnapshotAndSave
import pandas as pd 


class User():

	def __init__(self):
		self.db= config.mongo.db

	def create(self):
		try:
			
			self.userId = raw_input('Insira a identificação do usuário:')
			self.userName = raw_input('Insira o nome:')
			self.userReason = raw_input('Insira o que o motivo da utilização do linf:')
			print 'Olhe para câmera, pois iremos tirar algumas fotos!'
			arrayPhotos = TakeSnapshotAndSave(self.userId)

			photosCsv = pd.DataFrame(arrayPhotos)
			photosCsv.to_csv(self.userId+'.csv')

			self.userRelationship = raw_input('Informe o tipo de relacionamento com a UnB:\n\
						1- Alunos,\n\
						2- Professores,\n\
						3- Funcionários da Limpeza,\n\
						4- Seguranças,\n\
						5- Suporte Técnico,\n\
						6- Terceirizados.\n')



			self.db.Users.insert_one(
			{
				"_id": self.userId,
				"name":self.userName,
				"reason":self.userReason,
				"photos":arrayPhotos,
				"relationship":self.userRelationship,
			})

			print ('Usuário cadastrado com sucesso!')

		except Exception as e:
			print e.message

	def update(self):

		try:

			self.userId = raw_input('Informe o número de identificação do usuário:')

			self.option = raw_input('O que você deseja alterar?\n\
							1- Número de identificação,\n\
							2- Nome,\n\
							3- Motivo da utilização do Linf,\n\
							4- Fotos,\n\
							5- Relacionamento com a universidade')

			if self.option=='1':
				self.userId = raw_input('Insira a identificação do usuário:')
				self.db.Users.update(
				{"_id": self.userId},
					{
					"$set": {
						"_id":self.userId,
					}
				}
			)
				# print('\nRecords updated successfully\n')

			elif self.option=='2':
				self.userName = raw_input('Insira o nome:')
				self.db.Users.update(
				{"_id": self.userId},
					{
					"$set": {
						"name":self.userName,
					}
				}
			)
				#print("\nRecords updated successfully\n")

			elif self.option=='3':
				self.userReason = raw_input('Insira o que o motivo da utilização do linf:')
				self.db.Users.update(
				{"_id": self.userId},
					{
					"$set": {
						"reason":self.userReason,
					}
				}
			)
				# print ("\nRecords updated successfully\n")

			elif self.option=='4':
				self.userPhoto = raw_input('Informe o novo local das fotos:')
				self.db.Users.update(
				{"_id": self.userId},
					{
					"$set": {
						"photos":self.userPhoto,
					}
				}
			)


			elif self.option=='5':
				self.userRelationship = raw_input('Informe o tipo de relacionamento com a UnB:\n\
								  1- Alunos,\n\
								  2- Professores,\n\
								  3- Funcionários da Limpeza,\n\
								  4- Seguranças,\n\
								  5- Suporte Técnico,\n\
								  6- Terceirizados.')
				self.db.Users.update(
				{"_id": self.userId},
					{
					"$set": {
						"relationship":self.userRelationship,
					}
				}
			)


		except Exception as e:
			print e.message

	def read(self):
		try:
			self.userAll = self.db.Users.find()
			i =0
			print('Esses são os seus usuário:\n')

			for self.users in self.userAll:
				i +=1
				print 'Cadastro:',i

				if self.users['relationship'] =='1':
					self.users['relationship'] = 'Estudante'
				elif self.users['relationship']=='2':
					self.users['relationship'] = 'Docente'
				elif self.users['relationship']=='3':
					self.users['relationship']= 'Funcionário da Limpeza'
				elif self.users['relationship']=='4':
					self.users['relationship']='Segurança'
				elif self.users['relationship']=='5':
					self.users['relationship']='Suporte Técnico'
				else:
					self.users['relationship']='Terceirizado'

				print 'Identificador:',self.users['_id']
				print 'Nome:',self.users['name']
				print 'Motivo:',self.users['reason']
				print 'Fotos:', self.users['photos']
				print 'Relacionamento com a universidade:',self.users['relationship']
				print '\n'

		except Exception as e:
			print e.message

	def delete(self):
		try:
			self.userId = raw_input('Insira o id do usuário que deseja excluir\n')
			self.db.Users.delete_many({"_id":self.userId})
			print ('\nUsuário excluido com sucesso\n')
		except Exception as e:
			print e.message


