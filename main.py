#coding: utf-8
from user import User
from reservation import Reservation
import mongoconfig as db

class Main(User,Reservation):

    def main(self):

        while True:
            self.selection = raw_input('1- Para acessar o banco de usuários,\n2- Para acessar o banco de reservas,\n0- Para sair.\n')

            if self.selection == '1': 

                self.option = raw_input('Selecione:\n1 para cadastro\n2 para editar cadastro\n3 para visualizar usuários cadastrados\n4 para deletar um usuário\n0 para voltar ao menu principal\n')

                if self.option == '1':
                   User.create(self)
                elif self.option == '2':
                    User.update(self)
                elif self.option == '3':
                    User.read(self)
                elif self.option == '4':
                    User.delete(self)
                elif self.option =='0':
                    break
                else:
                    print ('Selecione uma opção válida.\n')

            elif self.selection == '2':

                self.option = raw_input('Selecione:\n 1 para reservar um horário\n2 para editar uma reserva\n3 para visualizar reservas cadastradas\n4 para deletar uma reserva\n0 para voltar ao menu principal\n')


                if self.option == '1':
                   Reservation.createReserve(self)
                elif self.option == '2':
                    Reservation.updateReserve(self)
                elif self.option == '3':
                    Reservation.readReserve(self)
                elif self.option == '4':
                    Reservation.deleteReserve(self)
                elif self.option =='0':
                    break
                else:
                    print ('Selecione uma opção válida.\n')          


            elif self.selection =='0':
                break

            else:
                pass

main = Main()
main.main()