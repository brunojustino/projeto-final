import numpy as np
import cv2
import time

#import the cascade for face detection
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def TakeSnapshotAndSave(user_id):

    arrayPhotos = []
    cap = cv2.VideoCapture(0)

    num = 0 
    while num<10:
        # Capture frame-by-frame
        ret, frame = cap.read()

        # to detect faces in video
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        for (x,y,w,h) in faces:
            cv2.rectangle(frame,(x,y),(x+w,y+h),(255,255,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = frame[y:y+h, x:x+w]

        x = 0
        y = 20

        cv2.imwrite(user_id+'_'+str(num)+'.jpg',frame)
        arrayPhotos.append(user_id+'_'+str(num)+'.jpg')

        num = num+1

    
    cap.release()
    cv2.destroyAllWindows()

    return arrayPhotos




