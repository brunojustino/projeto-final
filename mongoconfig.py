#coding: utf-8
#!/usr/bin/env python

from pymongo import MongoClient
import os, sys

class DatabaseConfig():
    client = MongoClient('localhost:27017')
    db = client.UserData

mongo = DatabaseConfig()