#coding: utf-8
from user import User
from pymongo import MongoClient
import os, sys
import mongoconfig as config

class Reservation(User):

	def __init__(self):
		self.db = config.mongo.db
	
	def createReserve(self):

		try:
			self.user_id = raw_input('Insira a identificação do autor da reserva: \n')
			self.reason = raw_input('Insira o motivo da reserva\
									\n 1- Aula,\
									\n 2- Limpeza\
									\n 3- Manutenção.\n')

			self.dayWeek 	  = raw_input('Insira os dias da semana que deseja reservar:\n')
			self.initialTime  = raw_input('Insira o horário de início\n')
			self.finalTime    = raw_input('Insira o horario de término\n')
			self.bool         = raw_input('A reserva é recorrente? 1-Sim e 2-Não \n')


			if(self.reason=='1'):
				self.nameSubject = raw_input('Insira o nome da matéria\n')
				self.codeSubject = raw_input('Insira o código da matéria\n')
				self.classe      = raw_input('Insira a turma\n')

				

				self.people = []

				while True:
					people = raw_input('Insira o nome dos alunos que irão estar nessa reserva\
										\n Digite 0 para cancelar seu cadastro:\n')

					if people!='0':
						self.people.append(people)

					else:
						break

				self.user = self.db.Users.find_one({"_id":self.user_id})

				self.db.Reservation.insert_one(
				{
					"user_id": self.user_id,
					"reason":'Aula',
					"subject":self.nameSubject,
					"codeSubject":self.codeSubject,
					"class":self.classe,
					"dayWeek":self.dayWeek,
					"initialTime":self.initialTime,
					"finalTime":self.finalTime,
					"bool": self.bool,
					"people":self.people
				})

				print(self.user['name']+ ' sua reserva foi concluida com sucesso') 

			if(self.reason=='2'):

				self.people = []

				self.user = self.db.Users.find_one({"_id":self.user_id})

				while True:
					people = raw_input('Insira o nome dos alunos que irão estar nessa reserva\
										\n Digite 0 para cancelar seu cadastro:\n')

					if people!='0':
						self.people.append(people)

					else:
						break
				self.db.Reservation.insert_one(
				{
					"user_id": self.user_id,
					"reason":'Limpeza',
					"dayWeek":self.dayWeek,
					"initialTime":self.initialTime,
					"finalTime":self.finalTime,
					"bool": self.bool,
					"people":self.people
				})

				print(self.user['name']+ ' sua reserva foi concluida com sucesso') 

			if(self.reason=='3'):

				self.people = []

				self.user = self.db.Users.find_one({"_id":self.user_id})

				while True:
					people = raw_input('Insira o nome dos alunos que irão estar nessa reserva\
										\n Digite 0 para cancelar seu cadastro:\n')

					if people!='0':
						self.people.append(people)

					else:
						break
				self.db.Reservation.insert_one(
				{
					"user_id": self.user_id,
					"reason":'Manutenção',
					"dayWeek":self.dayWeek,
					"initialTime":self.initialTime,
					"finalTime":self.finalTime,
					"bool": self.bool,
					"people":self.people
				})
				print(self.user['name']+ ' sua reserva foi concluida com sucesso') 



		except Exception as e:
			print e.message

	def updateReserve(self):

		try:

			self.user_id = raw_input('Informe o número de identificação do usuário:')

			self.option = raw_input('O que você deseja alterar?\n\
							1- Nome da Matéria,\n\
							2- Código da Matéria,\n\
							3- Turma da Matéria,\n\
							4- Horário inicial da reserva,\n\
							5- Horário de término da reserva,\n\
							6- Pessoas que irão participar da reserva.\n')

			if self.option=='1':
				self.nameSubject = raw_input('Insira o nome da disciplina:\n')
				self.db.Reservation.update(
				{"user_id": self.userId},
					{
					"$set": {
						"nameSubject":self.nameSubject,
					}
				}
			)
				print('\nAlterado com sucesso!\n')

			elif self.option=='2':
				self.codeSubject = raw_input('Insira o código da disciplina:\n')
				self.db.Reservation.update(
				{"user_id": self.userId},
					{
					"$set": {
						"codeSubject":self.codeSubject,
					}
				}
			)
				print('\nAlterado com sucesso!\n')

			elif self.option=='3':
				self.classe = raw_input('Insira a turma:')
				self.db.Reservation.update(
				{"user_id": self.userId},
					{
					"$set": {
						"class":self.classe,
					}
				}
			)
				print('\nAlterado com sucesso!\n')

			elif self.option=='4':
				self.initialTime = raw_input('Informe o novo horário de início:\n')
				self.db.Reservation.update(
				{"user_id": self.userId},
					{
					"$set": {
						"initialTime":self.initialTime,
					}
				}
			)
				print('\nAlterado com sucesso!\n')

			elif self.option=='5':
				self.finalTime = raw_input('Inisira o novo horário de término:\n')
				self.db.Reservation.update(

				{"user_id": self.userId},
					{
					"$set": {
						"finalTime":self.finalTime,
					}
				}
			)
				print('\nAlterado com sucesso!\n')

			elif self.option=='6':

				self.peoples = self.db.Reservation.find_one({"user_id":self.user_id})

				self.peoples = self.peoples['people']

				while True:
					people = raw_input('Insira o nome dos novos alunos que irão estar nessa reserva\
										\n Digite 0 para cancelar seu cadastro:\n')

					if people!='0':
						self.peoples.append(people)

					else:
						break

				self.db.Reservation.update(

				{"user_id": self.user_id},
					{
					"$set": {
						"people":self.peoples,
					}
				}
			)

				print('\nAlterado com sucesso!\n')


		except Exception as e:
			print e.message

	def readReserve(self):

		try:
			self.reserveAll = self.db.Reservation.find()
			i =0
			print('Esses são as reservas:\n')

			for self.reservation in self.reserveAll:
				i +=1
				print 'Reserva:',i

				print 'Motivo:',self.reservation['reason']
				print 'Reservado para o(s) dia(s):',self.reservation['dayWeek']
				print 'Horário de Início:',self.reservation['initialTime']
				print 'Horário de Término:',self.reservation['finalTime']
				print 'Pessoas cadastradas para esse horário'
				for people in self.reservation['people']:
					print people
				if(self.reservation['subject']):
					print 'A matéria para esse horário é :', self.reservation['subject']
					print 'A turma é : ', self.reservation['class']
					print 'O código da disciplina é:', self.reservation['codeSubject']

				print '\n'

		except Exception as e:
			print e.message

	def deleteReserve(self):
		try:
			self.userId = raw_input('Insira o id do usuário que deseja excluir a reserva\n')
			self.db.Reservation.delete_many({"user_id":self.userId})
			print ('Reserva Excluída com sucesso.\n')
		except Exception as e:
			print e.message
