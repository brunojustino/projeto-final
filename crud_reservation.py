# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'crud_reservation.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(600, 400)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(250, 360, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.dateEdit = QtGui.QDateEdit(Dialog)
        self.dateEdit.setGeometry(QtCore.QRect(180, 130, 110, 27))
        self.dateEdit.setObjectName(_fromUtf8("dateEdit"))
        self.timeEdit = QtGui.QTimeEdit(Dialog)
        self.timeEdit.setGeometry(QtCore.QRect(180, 170, 118, 27))
        self.timeEdit.setObjectName(_fromUtf8("timeEdit"))
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(40, 30, 131, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.lineEdit = QtGui.QLineEdit(Dialog)
        self.lineEdit.setGeometry(QtCore.QRect(180, 30, 113, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.label_2 = QtGui.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(40, 70, 111, 21))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.comboBox = QtGui.QComboBox(Dialog)
        self.comboBox.setGeometry(QtCore.QRect(180, 70, 121, 31))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(40, 140, 61, 20))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(40, 180, 101, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(Dialog)
        self.label_5.setGeometry(QtCore.QRect(40, 220, 111, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.timeEdit_2 = QtGui.QTimeEdit(Dialog)
        self.timeEdit_2.setGeometry(QtCore.QRect(180, 210, 118, 27))
        self.timeEdit_2.setObjectName(_fromUtf8("timeEdit_2"))
        self.label_6 = QtGui.QLabel(Dialog)
        self.label_6.setGeometry(QtCore.QRect(40, 270, 81, 21))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.radioButton = QtGui.QRadioButton(Dialog)
        self.radioButton.setGeometry(QtCore.QRect(140, 270, 98, 20))
        self.radioButton.setObjectName(_fromUtf8("radioButton"))
        self.radioButton_2 = QtGui.QRadioButton(Dialog)
        self.radioButton_2.setGeometry(QtCore.QRect(210, 270, 98, 20))
        self.radioButton_2.setObjectName(_fromUtf8("radioButton_2"))
        self.label_7 = QtGui.QLabel(Dialog)
        self.label_7.setGeometry(QtCore.QRect(40, 310, 191, 16))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.lineEdit_2 = QtGui.QLineEdit(Dialog)
        self.lineEdit_2.setGeometry(QtCore.QRect(230, 310, 341, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.radioButton_3 = QtGui.QRadioButton(Dialog)
        self.radioButton_3.setGeometry(QtCore.QRect(500, 30, 98, 20))
        self.radioButton_3.setObjectName(_fromUtf8("radioButton_3"))
        self.label_8 = QtGui.QLabel(Dialog)
        self.label_8.setGeometry(QtCore.QRect(330, 30, 81, 21))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.radioButton_4 = QtGui.QRadioButton(Dialog)
        self.radioButton_4.setGeometry(QtCore.QRect(430, 30, 98, 20))
        self.radioButton_4.setObjectName(_fromUtf8("radioButton_4"))
        self.label_9 = QtGui.QLabel(Dialog)
        self.label_9.setGeometry(QtCore.QRect(330, 70, 111, 16))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.label_10 = QtGui.QLabel(Dialog)
        self.label_10.setGeometry(QtCore.QRect(340, 110, 52, 15))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.label_11 = QtGui.QLabel(Dialog)
        self.label_11.setGeometry(QtCore.QRect(340, 150, 52, 15))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.lineEdit_3 = QtGui.QLineEdit(Dialog)
        self.lineEdit_3.setGeometry(QtCore.QRect(450, 60, 141, 27))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.lineEdit_4 = QtGui.QLineEdit(Dialog)
        self.lineEdit_4.setGeometry(QtCore.QRect(390, 100, 141, 27))
        self.lineEdit_4.setObjectName(_fromUtf8("lineEdit_4"))
        self.lineEdit_5 = QtGui.QLineEdit(Dialog)
        self.lineEdit_5.setGeometry(QtCore.QRect(390, 140, 61, 27))
        self.lineEdit_5.setObjectName(_fromUtf8("lineEdit_5"))

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Cadastro Reserva", None))
        self.label.setText(_translate("Dialog", "Identificação do autor:", None))
        self.label_2.setText(_translate("Dialog", "Motivo da reserva:", None))
        self.comboBox.setItemText(0, _translate("Dialog", "Aula", None))
        self.comboBox.setItemText(1, _translate("Dialog", "Limpeza", None))
        self.comboBox.setItemText(2, _translate("Dialog", "Manutenção", None))
        self.label_3.setText(_translate("Dialog", "Data:", None))
        self.label_4.setText(_translate("Dialog", "Horário de início:", None))
        self.label_5.setText(_translate("Dialog", "Horário de término:", None))
        self.label_6.setText(_translate("Dialog", "É recorrente?", None))
        self.radioButton.setText(_translate("Dialog", "Sim", None))
        self.radioButton_2.setText(_translate("Dialog", "Não", None))
        self.label_7.setText(_translate("Dialog", "Pessoas participantes da reserva:", None))
        self.radioButton_3.setText(_translate("Dialog", "Não", None))
        self.label_8.setText(_translate("Dialog", "É disciplina?", None))
        self.radioButton_4.setText(_translate("Dialog", "Sim", None))
        self.label_9.setText(_translate("Dialog", "Nome da disciplina:", None))
        self.label_10.setText(_translate("Dialog", "Código:", None))
        self.label_11.setText(_translate("Dialog", "Turma:", None))

