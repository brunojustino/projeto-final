# Trabalho 2 - Face Detection


## Introdução

Este trabalho utiliza princípios de visão computacional para criar um sistema
de controle de acesso para ser utilizado na entrada do Laboratório de Informática.
O sistema classifica as pessoas que acessam as salas do laboratório por categorias entre
alunos, funcionários da limpeza, suporte técnico, seguranças, professores e público externo.
    
No momento o sistema faz o cadastramento dos usuários e insere no banco de dados Não-Relacional,
no caso, MongoDB. 
    
Para casa pessoa cadastrada o sistema armazena o número de identificação do usuário, 
que pode ser matrícula da UnB ou cpf para não alunos ou servidores da universidade, 
nome completo, identificador de reserva, vetor de fotos e relacionamento com a universidade.

## Requisitos para funcionamento

O trabalho foi desenvolvido utilizando o sistema operacional Ubuntu 16.04.
A linguagem de programação Python foi utilizada para implementar o sistema de cadastros
neste projeto. Para isso, é necessário fazer instalações tanto para gerenciar as dependências
do Python, quanto para o Open CV. Abaixo estão listados os passos para instalação.

1. Instalar o gerenciador de dependências python
   
    ```  sudo apt-get install python-pip```

2. Instalar o Docker engine

    ```sudo apt-get update```

    ```sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D```
    
    ```sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main```
    
    ```sudo apt-get update```
    
    ```apt-cache policy docker-engine```
    
    ```sudo apt-get install -y docker-engine```
    
    ```sudo usermod -aG docker $(whoami)```
    
<b>Após instalar o docker, você precisa encerrar a sua sessão no sistema operacional e logar novamente para utilizá-lo.

3. Instalar o docker-compose
    
    ```sudo curl -o /usr/local/bin/docker-compose -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)"```

    ```sudo chmod +x /usr/local/bin/docker-compose```

4. Instalar as dependências do projeto

    ``` sudo pip install -r requirements.txt```

5. Instalar a biblioteca OpenCV a partir do Bash
    
    [Instalação OpenCV](https://github.com/brunojus/bash-opencv)
6. Para executar o programa digitar no terminal <br>
    ```docker-compose up mongo``` <br>
    Após isso você deverá abrir outro terminal e digitar esse comando.<br>
    ``` python main.py```

    
## Screenshot das telas

### Tela Principal

![Alt text](paginainicialui.png?raw=true "Tela Principal")

### Tela de cadastro de usuário

![Alt text](crud_user.png?raw=true "Cadastro de Usuário")

### Tela de cadastro de reservas

![Alt text](crud_reservation.png?raw=true "Cadastro de Reservas")

### Tela para exclusão de usuários

![Alt text](excluir_usuario.png?raw=true "Excluir Usuário")

### Tela para exclusão de reservas

![Alt text](excluir_reserva.png?raw=true "Excluir Reserva")

### Tela para edição de usuários

![Alt text](editar_usuario.png?raw=true "Editar Usuário")

### Tela para edição de reservas

![Alt text](editar_reserva.png?raw=true "Editar Reservas")

### Tela para visualização de usuário

![Alt text](visualizar_usuario.png?raw=true "Visualizar Usuário")

### Tela para visualização de reservas

![Alt text](visualizar_cadastro.png?raw=true "Visualizar Reservas")

### Tela de detecção de face

![Alt text](detector_face.jpg?raw=true "Detecção de face")


# Documentação usando Doxygen
 [Doxygen](html/index.html)
    
# Diagrama de Sequência

 ![Alt text](sequenc_diagram.png?raw=true "Diagrama de Sequência")

# Diagrama de Classes

![Alt text](html/classmain_1_1Main__inherit__graph.png?raw=true "Diagrama de Classes")





